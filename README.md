Primeiro vamos iniciar o projeto, digite o comando abaixo dentro da pasta do projeto:

`git init`

Depois, vamos criar um arquivo chamado README.md, e vamos adicionado ao projeto GIT:

`git add README.md`

Depois vamos fazer o commit do arquivo:

`git commit -m 'Adicionado o arquivo README.md'`

Dessa forma, estaremos com o arquivo adicionado ao GIT já com a mensagem relacionada ao commit dele.

Caso seja necessário fazer uma alteração no arquivo, ao digitar o comando:

`git status`

É possível ver que o arquivo está marcado como alterado e que devemos novamente adicioná-lo ao projeto, mas para 
não precisar digitar o commando `git add README.md` podemos simplesmente adicionar o parâmetro `-a` ao commit 
e já estaremos fazendo o commit e adicionando os arquivo modificados ao mesmo tempo:

`git commit -am 'Adicionadas mais informações ao README.md'`